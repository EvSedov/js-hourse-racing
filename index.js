const horseRaceGame = new HorseRacing();

function HorseRacing() {
  const PLAYER_DEFAULT_BANK = 1000;
  const MAX_TIME = 3000;
  const MIN_TIME = 500
  this.horses = ['Росинант', 'Буцефал', 'Кинг Джордж', 'Кумир'].map(
    (name) => new Horse(name)
  );
  this.player = new Player(PLAYER_DEFAULT_BANK);
  this.wagers = [];
  this.clearWagers = function () {
    this.wagers = [];
  };

  function Horse(name) {
    this.name = name;
    this.run = () => {
      const RANDOM_TIME = Math.floor(Math.random() * (MAX_TIME - MIN_TIME)) + MIN_TIME;
      return new Promise((resolve) => setTimeout(resolve, RANDOM_TIME));
    };
  }

  function Player(bank) {
    this.account = bank;
  }

  // здесь вы можете дописать различные вспомогательные функции
  // например, для проверки ставок, изменения счета игрока и т.д.
  this.setDefaultPlayarAccount = function () {
    this.player.account = PLAYER_DEFAULT_BANK;
  }

  this.showWagers = function () {
    if (!this.wagers.length) {
      console.log('Вы не сделали ни одной ставки')
    } else {
      console.log('Вы поставили на:');
      this.wagers.forEach(([name, wager]) => console.log(`${name} - ${wager}`));
    }
  }
}

function showHorses() {
  horseRaceGame.horses.forEach((horse) => console.log(horse.name));
}

function showAccount() {
  console.log(`Ваш счет равен: ${horseRaceGame.player.account}`);
}

function setWager(name, sumToBet) {
  if (!Number.isInteger(sumToBet) || sumToBet < 0) {
    console.log('Размер ставки должен быть целым положительным числом.')
    return
  }
  if (horseRaceGame.player.account - sumToBet < 0) {
    console.error('У вас недостаточно денег');
    return
  }
  horseRaceGame.player.account -= sumToBet;
  horseRaceGame.wagers.push([name, sumToBet]);
}

async function startRacing() {
  // начать забег
  let winCount = 0;

  const racedHorses = horseRaceGame.horses.map(
    (horse) => horse.run().then(() => {
      console.log(horse.name);
      return horse.name;
    })
  );

  const winHorse = await Promise.race(racedHorses);
  await Promise.all(racedHorses);
  
  horseRaceGame.wagers.forEach(([nameHorse, wager]) => {
    if (nameHorse == winHorse) {
      winCount = wager * 2;
    }
  });
  if (winCount) {
    horseRaceGame.player.account += winCount;
    console.log(`Вы выиграли ${winCount}, текущий счет ${horseRaceGame.player.account}.`);
    winCount = 0;
  } else {
    showAccount();
  }
  horseRaceGame.clearWagers();
};

function newGame() {
  // восстановить исходный счет игрока
  horseRaceGame.setDefaultPlayarAccount();
  horseRaceGame.clearWagers();
  console.log('Вы начали новую игру.');
  showAccount();
  console.log('Список лошадей:');
  showHorses();
  console.log('Делайте ваши ставки!!!');
};
